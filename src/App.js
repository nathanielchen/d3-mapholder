import React, { Component } from "react";
import "./App.css";
import Map from "./pages/Map/index";

class App extends Component {
  constructor(props: Props) {
    super(props);
    this.state = {
      mapData: null
    };
  }

  render() {
    return (
      <div className="app">
        <Map
          title="United States Map of Gender Population by State (2016)"
          description="Hover over a state to view their official gender population and click to
    view gender and total population."
          message="Select a State"
          year={2016}
          defaultNode="total"
          defaultFills={[
            "#CDEC51",
            "#93D243",
            "#80C43B",
            "#6FB734",
            "#5BA72B",
            "#429321",
            "#5B703F",
            "#715158",
            "#59323B"
          ]}
          domains={[
            0,
            580000,
            2800000,
            5280000,
            8260000,
            11600000,
            19600000,
            26500000,
            38400000
          ]}
          mapPath="../assets/map/usStatePaths.json"
          csv="../assets/data/gender2016.csv"
        />

        <Map
          title="United States Map of Population by State (2010 - 2017)"
          description="Hover over a state to view their last official population and click to
view the change of population between (2010 - 2017)"
          message="Select a State"
          year={2017}
          defaultFills={[
            "#eeeeee",
            "#d8e1ea",
            "#bdcad9",
            "#a3b5ca",
            "#889fbb",
            "#6d89ab",
            "#51739b",
            "#325a89",
            "#05366f"
          ]}
          domains={[
            0,
            580000,
            2800000,
            5280000,
            8260000,
            11600000,
            19600000,
            26500000,
            38400000
          ]}
          yearRange={[2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]}
          mapPath="../assets/map/usStatePaths.json"
          csv="../assets/data/years2010-2017.csv"
        />
        <Map
          title="United States Map of Population by State (2014 - 2017)"
          description="Hover over a state to view their official population and click to
view the change of population between (2014 - 2017)"
          message="Select a State"
          year={2014}
          defaultFills={[
            "#F7EBC2",
            "#F7CE9C",
            "#F7B57B",
            "#F79D5C",
            "#F7823A",
            "#F76B1C",
            "#E3431F",
            "#D22121",
            "#C20024"
          ]}
          domains={[
            0,
            580000,
            2800000,
            5280000,
            8260000,
            11600000,
            19600000,
            26500000,
            38400000
          ]}
          yearRange={[2014, 2015, 2016, 2017]}
          mapPath="../assets/map/usStatePaths.json"
          csv="../assets/data/years2010-2017.csv"
        />

        <Map
          title="United States Map of Population by State (2010)"
          description="Hover over a state to view their official population and click to
view the population in side veiw."
          message="Select a State"
          year={2010}
          width={"100%"}
          height={"100%"}
          defaultFills={[
            "#CEE1E8",
            "#3FCCFF",
            "#38C1F4",
            "#32B5E8",
            "#2BA9DC",
            "#239BCE",
            "#1B8DC0",
            "#1380B2",
            "#0A70A3"
          ]}
          domains={[
            0,
            580000,
            2800000,
            5280000,
            8260000,
            11600000,
            19600000,
            26500000,
            38400000
          ]}
          mapPath="../assets/map/usStatePaths.json"
          csv="../assets/data/years2010-2017.csv"
        />

        <Map
          title="United States Map of Population by State (2011 - 2017)"
          description="Hover over a state to view their last official population and click to
view the change of population between (2011 - 2017)"
          message="Select a State"
          year={2011}
          width={"100%"}
          height={"100%"}
          defaultFills={[
            "#C86DD7",
            "#B865D3",
            "#A95ECF",
            "#9755CA",
            "#854CC5",
            "#7243C0",
            "#603ABB",
            "#4C30B5",
            "#3023AE"
          ]}
          domains={[
            0,
            580000,
            2800000,
            5280000,
            8260000,
            11600000,
            19600000,
            26500000,
            38400000
          ]}
          yearRange={[2011, 2012, 2013, 2014, 2015, 2016, 2017]}
          mapPath="../assets/map/usStatePaths.json"
          csv="../assets/data/years2010-2017.csv"
        />
      </div>
    );
  }
}

export default App;
/**/
