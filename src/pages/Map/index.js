import React, { Component } from "react";
import PropTypes from "prop-types";

import MapHolder from "../../components/MapHolder";
import MapInfo from "../../components/MapInfo";

class Map extends Component<Props> {
  constructor(props: Props) {
    super();
    this.state = {
      mapData: props.mapData
    };
  }

  handleSelectState = mapData => {
    this.setState({
      mapData: mapData
    });
  };

  render() {
    if (this.state.mapData) {
      this.MapInfo = (
        <MapInfo
          className="map-info"
          year={this.props.year}
          yearRange={this.props.yearRange}
          mapData={this.state.mapData}
          defaultNode={this.props.defaultNode}
        />
      );
    } else {
      this.MapInfo = (
        <div width={"100%"} height={"100%"} className="map-info">
          <div>{this.props.message}</div>
        </div>
      );
    }

    return (
      <div className="app">
        <h1>{this.props.title}</h1>
        <div className="container">
          <div className="row">
            <div className="col-md-9 col-sm-8 col-xs-12">
              <div className="map-container">
                <svg
                  viewBox="0 0 960 600"
                  preserveAspectRatio="xMinYMin slice"
                  className="map-svg"
                >
                  <MapHolder
                    {...this.props}
                    handleSelectState={this.handleSelectState}
                  />
                </svg>
              </div>
            </div>
            <div className=" col-md-3 col-sm-4 col-xs-12 ">{this.MapInfo}</div>
          </div>
        </div>
        <h5>{this.props.description}</h5>
      </div>
    );
  }
}

Map.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  message: PropTypes.string,
  year: PropTypes.number,
  defaultNode: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  defaultFills: PropTypes.array,
  domains: PropTypes.array,
  yearRange: PropTypes.array,
  mapPath: PropTypes.string,
  csv: PropTypes.string,
  mapData: PropTypes.object
};

Map.defaultProps = {
  title: "",
  description: "",
  message: "",
  year: 2000,
  defaultNode: "",
  width: "100%",
  height: "100%",
  defaultFills: [],
  domains: [],
  yearRange: [],
  mapPath: "",
  csv: "",
  mapData: null
};

export default Map;
