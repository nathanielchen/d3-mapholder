import React, { Component } from "react";
import PropTypes from "prop-types";
import * as d3 from "d3";
import "./MapInfo.css";

type Props = {
  mapData: Object,
  className: string
};

class MapInfo extends Component<Props> {
  constructor(props: Props) {
    super();
    this.state = {
      mapData: props.mapData
    };
  }

  findAverage(mapData) {
    let values = [];

    for (let i = 0; i < this.props.yearRange.length - 1; i++) {
      values.push(
        mapData[`y${this.props.yearRange[i + 1]}`] /
          mapData[`y${this.props.yearRange[i]}`] -
          1
      );
    }
    let sum = values.reduce((previous, current) => (current += previous));
    let rate = sum / values.length;
    return [
      <h3 key={this.props.yearRange}>
        ({this.props.yearRange[0]} -
        {this.props.yearRange[this.props.yearRange.length - 1]})
      </h3>,
      <p key={rate} className={rate > 0 ? "" : "red-text"}>
        Population Annual Change: {d3.format(".4f")(rate)}%
      </p>
    ];
  }

  displayPopulation(mapData) {
    return [
      <h3 key={this.props.year}>({this.props.year})</h3>,
      <h2 key={this.props.year}>
        {d3.format(",")(mapData[`y${this.props.year}`])}
      </h2>
    ];
  }

  render() {
    const mapData = this.props.mapData;

    var yearList = this.props.yearRange.map(function(year) {
      return (
        <tbody key={year}>
          <tr>
            <td className="table-year-value">{year}:</td>
            <td className="table-value">
              {d3.format(",")(mapData[`y${year}`])}
            </td>
          </tr>
        </tbody>
      );
    });

    if (this.props.defaultNode) {
      var genderTabble = (
        <tbody>
          <tr>
            <td className="table-year-value">male:</td>
            <td className="table-value">{d3.format(",")(mapData.male)}</td>
          </tr>
          <tr>
            <td className="table-year-value">female:</td>
            <td className="table-value">{d3.format(",")(mapData.female)}</td>
          </tr>
          <tr>
            <td className="table-year-value">total:</td>
            <td className="table-value">{d3.format(",")(mapData.total)}</td>
          </tr>
        </tbody>
      );
    }

    if (!mapData) {
      return null;
    }
    return (
      <div className={this.props.className}>
        <h2 className="state-name">{mapData.n}</h2>
        {!this.props.defaultNode
          ? this.props.yearRange.length > 1
            ? this.findAverage(mapData)
            : this.displayPopulation(mapData)
          : null}
        <table className="info-table">
          {this.props.defaultNode ? genderTabble : yearList}
        </table>
      </div>
    );
  }
}

export default MapInfo;

MapInfo.propTypes = {
  year: PropTypes.number,
  yearRange: PropTypes.array,
  mapData: PropTypes.object,
  defaultNode: PropTypes.string
};

MapInfo.defaultProps = {
  year: 2000,
  yearRange: [],
  defaultNode: "",
  mapData: {}
};
