import React, { Component } from "react";
import PropTypes from "prop-types";
import * as d3 from "d3";
import "./MapHolder.css";

class USAState extends Component<Props> {
  constructor(props: Props) {
    super(props);
    console.log("this.props.mapData", this.props.mapData);
    this.state = {
      states: null,
      statsData: this.props.mapData,
      mergedData: null
    };

    this.tooltip = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip");

    this.legend = null;
  }

  mergeData = (d1, d1key, d2, d2key) => {
    let data = [];
    d1.forEach(s1 => {
      d2.forEach(s2 => {
        if (s1[d1key] === s2[d2key]) {
          data.push(Object.assign({}, s1, s2));
        }
      });
    });

    return data;
  };

  componentWillMount = () => {
    var sampleData = {}; /* Sample random data. */
    [
      "HI",
      "AK",
      "FL",
      "SC",
      "GA",
      "AL",
      "NC",
      "TN",
      "RI",
      "CT",
      "MA",
      "ME",
      "NH",
      "VT",
      "NY",
      "NJ",
      "PA",
      "DE",
      "MD",
      "WV",
      "KY",
      "OH",
      "MI",
      "WY",
      "MT",
      "ID",
      "WA",
      "DC",
      "TX",
      "CA",
      "AZ",
      "NV",
      "UT",
      "CO",
      "NM",
      "OR",
      "ND",
      "SD",
      "NE",
      "IA",
      "MS",
      "IN",
      "IL",
      "MN",
      "WI",
      "MO",
      "AR",
      "OK",
      "KS",
      "LS",
      "VA"
    ].forEach(function(d) {
      var randomA = Math.round(100 * Math.random());
      var randomB = Math.round(100 * Math.random());
      var i = 0;
      sampleData[i++] = {
        id: d,
        male: d3.min([randomA, randomB]),
        female: d3.max([randomA, randomB]) - d3.min([randomA, randomB]),
        total: d3.max([randomA, randomB])
      };
    });

    // function print_filter(filter) {
    //   var f = eval(filter);
    //   if (typeof f.length != "undefined") {
    //   } else {
    //   }
    //   if (typeof f.top != "undefined") {
    //     f = f.top(Infinity);
    //   } else {
    //   }
    //   if (typeof f.dimension != "undefined") {
    //     f = f
    //       .dimension(function(d) {
    //         return "";
    //       })
    //       .top(Infinity);
    //   } else {
    //   }
    //   console.log(
    //     filter +
    //       "(" +
    //       f.length +
    //       ") = " +
    //       JSON.stringify(f)
    //         .replace("[", "[\n\t")
    //         .replace(/}\,/g, "},\n\t")
    //         .replace("]", "\n]")
    //   );
    // }

    this.setState({
      statsData: sampleData
    });

    console.log("sampleData", sampleData);

    if (this.props.csv) {
      d3
        .queue()
        .defer(d3.json, this.props.mapPath)
        .defer(d3.csv, this.props.csv)
        .awaitAll((error, results) => {
          this.setState({
            states: results[0].usStatePaths,
            statsData: results[1],
            mergedData: this.mergeData(
              results[0].usStatePaths,
              "id",
              results[1],
              "id"
            )
          });
        });
    } else {
      d3
        .queue()
        .defer(d3.json, this.props.mapPath)
        .awaitAll((error, results) => {
          console.log("results", results);
          this.setState({
            states: results[0].usStatePaths,
            mergedData: this.mergeData(
              results[0].usStatePaths,
              "id",
              this.state.statsData,
              "id"
            )
          });
        });
    }
  };

  componentDidUpdate = () => {
    const scaleThreshold = d3
      .scaleThreshold()
      .domain(this.props.domains)
      .range(this.props.defaultFills);

    const scaleLinear = d3
      .scaleLinear()
      .domain(this.props.domains)
      .rangeRound([400, 408]);

    const map = d3.select(this.refs.usaAnchor);

    if (!this.legend) {
      this.legend = map
        .append("g")
        .attr("class", "legend")
        .attr("transform", "translate(10,0)");

      this.legend
        .selectAll("rect")
        .data(
          scaleThreshold.range().map(function(d) {
            d = scaleThreshold.invertExtent(d);
            if (d[0] == null) d[0] = scaleLinear.domain()[0];
            if (d[1] == null) d[1] = scaleLinear.domain()[1];
            return d;
          })
        )
        .enter()
        .append("rect")
        .attr("height", 8)
        .attr("x", function(d) {
          return scaleLinear(d[0]);
        })
        .attr("width", function(d) {
          return scaleLinear(d[1]) - scaleLinear(d[0]);
        })
        .attr("fill", function(d) {
          return scaleThreshold(d[0]);
        });

      this.legend
        .append("text")
        .attr("class", "caption")
        .attr("x", scaleLinear.range()[0])
        .attr("y", -6)
        .attr("fill", "#000")
        .attr("text-anchor", "start")
        .attr("font-weight", "bold")
        .text("Population by state");

      this.legend
        .append("svg:image")
        .attr("x", 830)
        .attr("y", 500)
        .attr("opacity", 0.8)
        .attr("width", 100)
        .attr("height", 100);

      this.legend
        .call(
          d3
            .axisBottom(scaleLinear)
            .tickSize(13)
            .tickFormat(function(x, i) {
              return x === 0
                ? ""
                : x > 1000000
                  ? x / 1000000 + "m"
                  : x / 1000 + "k";
            })
            .tickValues(scaleThreshold.domain())
        )
        .select(".domain")
        .remove();
    }

    const createTip = d => {
      let tooltip = null;
      if (this.props.defaultNode !== "") {
        tooltip = `<h4>${d.n}</h4><p class="tooltip-info">${d3.format(",")(
          d[this.props.defaultNode]
        )}</p>`;
      } else {
        tooltip = `<h4>${d.n}</h4><p class="tooltip-info">${d3.format(",")(
          d[`y${this.props.year}`]
        )}</p>`;
      }

      return tooltip;
    };

    const rateById = {};
    this.state.mergedData.forEach(function(d) {
      if (this.props.defaultNode) rateById[d.id] = +d[this.props.defaultNode];
      else rateById[d.id] = +d[`y${this.props.year}`];
    }, this);

    map
      .selectAll(".state")
      .data(this.state.mergedData)
      .enter()
      .append("path")
      .attr("class", "state")
      .attr("d", function(d) {
        return d.d;
      })
      .style("fill", function(d) {
        return scaleThreshold(rateById[d.id]);
      })
      .on("mouseover", d => {
        this.tooltip
          .transition()
          .duration(200)
          .style("opacity", 0.9);
        this.tooltip
          .html(createTip(d))
          .style("left", `${d3.event.pageX}px`)
          .style("top", `${d3.event.pageY}px`);
      })
      .on("mouseout", () =>
        this.tooltip
          .transition()
          .duration(500)
          .style("opacity", 0)
      )
      .on("click", d => {
        this.props.handleSelectState(d);
      });
  };

  render() {
    const { mergedData } = this.state;

    if (!mergedData) {
      return null;
    }
    return <g ref="usaAnchor" />;
  }
}

export default USAState;

USAState.propTypes = {
  year: PropTypes.number,
  defaultNode: PropTypes.string,
  defaultFills: PropTypes.array,
  domains: PropTypes.array,
  mapPath: PropTypes.string,
  csv: PropTypes.string,
  mapData: PropTypes.object
};

USAState.defaultProps = {
  year: 2000,
  defaultNode: "",
  defaultFills: [
    "#eeeeee",
    "#dddddd",
    "#cccccc",
    "#bbbbbb",
    "#aaaaaa",
    "#999999",
    "#888888",
    "#777777",
    "#666666"
  ],
  domains: [1, 2, 3, 4, 5, 6, 7, 8, 9],
  mapPath: "",
  csv: "",
  mapData: {}
};
